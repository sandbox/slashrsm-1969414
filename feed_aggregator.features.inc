<?php
/**
 * @file
 * feed_aggregator.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feed_aggregator_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function feed_aggregator_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function feed_aggregator_node_info() {
  $items = array(
    'aggregator_item' => array(
      'name' => t('Aggregator item'),
      'base' => 'node_content',
      'description' => t('Single article/blog/news fetched from a subscribed feed.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'subscription' => array(
      'name' => t('Subscription'),
      'base' => 'node_content',
      'description' => t('Single RSS aggregator subscription feed.'),
      'has_title' => '1',
      'title_label' => t('Feed name'),
      'help' => '',
    ),
  );
  return $items;
}
